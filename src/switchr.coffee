Switchr = (element, options) ->
    @options = options
    @$element = $(element)
    @$container = $("<div class='switchr-container'></div>")
    @$bar = $("<div class='switchr-bar'></div>")
    @$slider = $("<div class='switchr-slider'></div>")
    @$text = $("<div class='switchr-text'>Off</div>")
    @init()
    return
  
Switchr:: =
  lastSliderPosition: null
  
  init: ->
    that = @
    # Hide the original input checkbox
    @$element.css({ position: 'absolute', left: -9999, display: 'none'})
    
    # Prepare the slider for the DOM
    @$container.append(@$bar)
    @$bar.append(@$text)
    @$bar.append(@$slider)
    
    # Add to DOM
    @$element.after(@$container)
    
    barGrid = 0
    
    @$element.on('change', (e) ->
      $(@).prop("checked", !$(@).prop("checked"))
      that.moveSliderTo(barGrid, false)
      return
    )
    
    @$bar.on('click', (e) ->
      that.moveSliderTo(barGrid, true)
      return
    )
    null
      
  sliderPosition: (barGrid) ->
    add = 0
    
    if (@$element.is(':checked'))
      add = @$bar.innerWidth() - @$slider.outerWidth(true)
    else
      add = 0

    return barGrid + add

  animateBackground: ->
    if (@$element.is(':checked'))
      @$bar.css({
        backgroundColor: "#5C71E0",
      })
      @$text.text("On")
    else
      @$bar.css({
        backgroundColor: "#E04C4C",
      })
      @$text.text("Off")

  moveSliderTo: (barGrid, triggerChange) ->
    leftPosition = @sliderPosition(barGrid)
    
    if (leftPosition != null)
      @$slider.animate({
        left: leftPosition
      }, 100)
    
    @animateBackground()
            
    if (triggerChange)
      @$element.trigger('change')
    return
  
#jQuery Plugin Definition
jQuery.fn.switchr = (option) ->
  @each ->
    $this = $(this)
    options = $.extend({}, $.fn.switchr.defaults, typeof option is "object" and option)
    new Switchr(this, options)


jQuery.fn.switchr.defaults = draggable: true
jQuery.fn.switchr.Constructor = Switchr

window.Switchr = Switchr

